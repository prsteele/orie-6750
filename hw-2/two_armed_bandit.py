"""
File: two_armed_bandit.py

"""

from bandit import Bandit

class TwoArmedBandit(Bandit):
    """
    A Bandit with two unknown arms.
    

    """

    def __init__(self, T, alpha, beta, b, gamma):
        self.T = T
        self.alpha = alpha
        self.beta = beta
        self.b = b
        self.gamma = gamma

        self._memo = {}


    def _V(self, t, alpha, beta):

        if t >= self.T:
            raise ValueError("Invalid time %i" % t)

        mean = alpha / (alpha + beta)

        if t == self.T - 1:
            zero = 2 * self.b
            one = self.b + mean
            two = 2 * mean
        else:
            vt = self.V(t + 1, alpha, beta)
            vt_s = mean * self.V(t + 1, alpha + 1, beta)
            vt_f = (1 - mean) * self.V(t + 1, alpha, beta + 1)

            norm = (alpha + beta) * (alpha + beta + 1)
            p_0 = beta * (beta + 1) / norm
            p_1 = 2 * alpha * beta / norm
            p_2 = alpha * (alpha + 1) / norm
            
            vt_ss = p_2 * self.V(t + 1, alpha + 2, beta)
            vt_sf = p_1 * self.V(t + 1, alpha + 1, beta + 1)
            vt_ff = p_0 * self.V(t + 1, alpha, beta + 2)

            zero = 2 * self.b + self.gamma * vt
            one = self.b + mean + self.gamma * (vt_s + vt_f)
            two = 2 * mean + self.gamma * (vt_ss + vt_sf + vt_ff)

        if zero >= one and zero >= two:
            return (zero, 0)
        elif one >= two:
            return (one, 1)
        else:
            return (two, 2)
                                          
            
