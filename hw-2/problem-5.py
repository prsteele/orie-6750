"""
File: problem-5.py

"""

from matplotlib import pyplot as plt
from matplotlib import rc
rc("font", **{"family": "serif"})
rc("text", usetex=True)

from two_armed_bandit import TwoArmedBandit

T = 25
alpha = 1
beta = 1
b = .5
gamma = .95

bandit = TwoArmedBandit(T=T, alpha=alpha, beta=beta, b=b, gamma=gamma)

print(bandit.V(0, alpha, beta))

#
# Problem 5b
#


plays = {0: {"alpha": [], "beta": []},
         1: {"alpha": [], "beta": []},
         2: {"alpha": [], "beta": []}}

t = 10
for a in range(0, 2 * t + 1):
    for b in range(0, 2 * t - a + 1):
        _alpha = alpha + a
        _beta = beta + b
        k = bandit.policy(t, _alpha, _beta)
        plays[k]["alpha"].append(_alpha)
        plays[k]["beta"].append(_beta)

p1 = plt.scatter(plays[0]["alpha"], plays[0]["beta"], marker="*")
p2 = plt.scatter(plays[1]["alpha"], plays[1]["beta"], marker="s")
p3 = plt.scatter(plays[2]["alpha"], plays[2]["beta"], marker="+")
plt.title("$(\\alpha_t, \\beta_t)$ accessible at time $t = 10$")
plt.xlim((0, 2 * t + 2))
plt.ylim((0, 2 * t + 2))
plt.xlabel("$\\alpha_t$")
plt.ylabel("$\\beta_t$")
plt.legend([p1, p2, p3], ["Known arm", "Single arm", "Double arm"])
plt.savefig("problem-5b.png")
