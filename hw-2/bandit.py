"""
File: bandit.py

"""

class Bandit:
    """
    This class represents a one-armed bandit

    """
    
    def __init__(self, T, alpha, beta, b, gamma):
        """
        Construct a new finite-horizon one-armed bandit with time
        horizon T, priors alpha and beta, known reward b, and discount
        factor gamma.

        """

        self.T = T
        self.alpha = alpha
        self.beta = beta
        self.b = b
        self.gamma = gamma
        
        self.KNOWN = 0
        self.UNKNOWN = 1

        self._memo = {}

    def V(self, t, a, b):
        """
        Computes V_t(a, b); returns the value of the optimal policy at
        time t under parameters a and b.

        """

        k = (t, a, b)
        
        if k not in self._memo:
            self._memo[k] = self._V(*k)

        return self._memo[k][0]

    def policy(self, t, a, b):
        k = (t, a, b)
        
        if k not in self._memo:
            self._memo[k] = self._V(*k)

        return self._memo[k][1]

    def istar(self):
        """
        Returns a list [i^*_1, ..., i^*_T], where i^*_t is the minimum
        number of successes needed to be observed at time t to pull
        the unknown arm.

        """

        return [self._istar(n) for n in range(0, self.T)]

    def _istar(self, t):
        """
        Computes and returns the value of i^*_t.

        """

        # We perform a binary search on the number of successes a to
        # find the minimum number of successes for which we pull the
        # unknown arm.

        L = 0
        U = t

        if self.policy(t, self.alpha + U, self.beta) == self.KNOWN:
            return U + 1

        while L < U:
            M = (L + U) // 2

            if self.policy(t, self.alpha + M, self.beta + t - M) == self.KNOWN:
                L = M + 1
            else:
                U = M

        return L

    def _V(self, t, a, b):
        """
        Helper function to self.V to perform actual computations; as
        self.V actually performs the memoization, _V should only
        directly recurse through V. Returns (v, x), where v is the
        value of the policy and x is the choice made.

        """

        if t >= self.T:
            raise ValueError("Invalid time %i" % t)

        if t == self.T - 1:
            known = self.b
            unknown = a / (a + b)
        else:
            ep = a / (a + b)
            known = self.b + self.gamma * self.V(t + 1, a, b)

            on_success = self.V(t + 1, a + 1, b)
            on_failure = self.V(t + 1, a, b + 1)

            unknown = ep + self.gamma * (ep * on_success + (1 - ep) * on_failure)

        if known > unknown:
            return (known, self.KNOWN)
        else:
            return (unknown, self.UNKNOWN)
