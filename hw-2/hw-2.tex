\documentclass{article}

\usepackage{graphicx}
\usepackage{prsteele}
\usepackage{fullpage}
\usepackage{listings}
\usepackage{algpseudocode}
\usepackage{psfrag}
\usepackage{url}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[babel=true]{microtype}

\usepackage{lmodern}

\title{ORIE 6750: Assignment 2}
\author{Patrick Steele}
\date{\today}

\lstset{%
  tabsize=2,
  showstringspaces=false,
  frame=single
}

\newcommand{\VOI}{\text{VOI}}

\begin{document}

\maketitle

\section*{Problem 1}

We consider the one-armed bandit problem with Bernoulli
rewards. Consider time horizon $T = 25$, prior parameters $\alpha_0 =
\beta_0 = 1$, known payoff $b = .5$, and discount factor $\gamma =
.95$. Using the Python\footnote{Throughout, Python refers to Python
  3.3.2.} code below, we find $V_0(\alpha, \beta) = 8.543$; a plot of
$i^*_t$ versus $t$ is shown in figure~\ref{fig:p1}.

\lstinputlisting[language=Python]{problem-1.py}

\noindent This code relies on the \texttt{bandit} module, listed in
the appendix.

\begin{figure}[htp]
  \centering

  \includegraphics[width=\linewidth]{problem-1b.png}

  \caption{A plot of $i^*_t$ versus $t$ using the parameters from
    problem~1.}
  \label{fig:p1}
\end{figure}

\section*{Problem 2}

We verify the optimal policy given from problem~1 via Monte Carlo
simulation. Using the Python code below, we compute that the expected
discounted reward under the optimal policy to be $8.537 \pm .177$ at
the 95\% confidence level, finding a variance of $8.169$.

\lstinputlisting[language=Python]{problem-2.py}

\section*{Problem 3}

\begin{prop}
  The expected discounted reward for the one-armed bandit with
  Bernoulli rewards in non-decreasing in $T$; that is, $V_0(\alpha,
  \beta)$ is non-decreasing in $T$.
\end{prop}

\begin{proof}
  We use backwards induction.

  \paragraph{Base case.} Let $\alpha, \beta > 0$ be given and observe
  that $V_{T - 1}(\alpha, \beta) = \max\cbrace{b, \alpha / (\alpha +
    \beta)}$ is independent from $T$, and so is non-decreasing in $T$.

  \paragraph{Inductive step.} Suppose for some $0 \le t < T - 1$ that $V_{t +
    1}(\alpha', \beta')$ is non-decreasing in $T$ for all $\alpha',
  \beta' > 0$. Let $\alpha, \beta > 0$ be given. Since $t < T$,
  \begin{equation*}
    V_t(\alpha, \beta) = \max\cbrace{b + \gamma V_{t + 1}(\alpha,
      \beta), \frac{\alpha}{\alpha + \beta} +
      \gamma \paren{\frac{\alpha}{\alpha + \beta} V_{t + 1}(\alpha + 1,
        \beta) + \frac{\beta}{\alpha + \beta} V_{t + 1}(\alpha, \beta
      + 1}}.
  \end{equation*}
  By the inductive hypothesis, $V_{t + 1}(\alpha, \beta)$ $V_{t +
    1}(\alpha + 1)$, and $V_{t + 1}(\alpha, \beta + 1)$ are all
  non-decreasing in $T$. Since $\alpha, \beta, \gamma, b > 0$, we have
  that $V_t(\alpha, \beta)$ is the maximum of two non-decreasing
  functions in $T$, and so $V_t(\alpha, \beta)$ is non-decreasing in
  $T$, as required.

  Hence, by induction, $V_0(\alpha, \beta)$ is non-decreasing in $T$.

\end{proof}

\section*{Problem 4}

\begin{prop}
  In the one-armed bandit problem with Bernoulli rewards, if $t < T$
  and $\alpha_t / (\alpha_t + \beta_t) > b$, then it is optimal to
  choose $x_{t + 1} = 1$.
\end{prop}

\begin{proof}
  We use induction on $t$.

  \paragraph{Base case.} Let $t = T - 1$. Then
  \begin{equation*}
    V_t(\alpha_t, \beta_t) = \max\cbrace{b, \frac{\alpha_t}{\alpha_t +
        \beta_t}} = \frac{\alpha_t}{\alpha_t + \beta_t}
  \end{equation*}
  by assumption; thus, it is optimal to choose $x_{t + 1} = 1$.

  \paragraph{Inductive step.} Assume for some $0 \le t < T - 1$ that
  when $\alpha_{t + 1} / (\alpha_{t + 1} + \beta_{t + 1}) > b$ that it
  is optimal to play $x_{t + 2} = 1$. Suppose for contradiction that
  $b < \alpha_t / (\alpha_t + \beta_t$, but it is optimal to play
  $x_{t + 1} = 0$. Since we play $x_{t + 1} = 0$, we have $\alpha_{t +
    1} = \alpha_t$ and $\beta_{t + 1} = \beta_t$. Thus, $\alpha_{t +
    1} / (\alpha_{t + 1} + \beta_{t + 1}) > b$, and so by inductive
  assumption $x_{t + 2} = 1$. This is a contradiction of the stopping
  time result we derived in class (lecture notes~9, first claim on
  page~2). Thus, $x_{t + 1} = 1$, as required.
\end{proof}

\section*{Problem 5}

We consider a two-armed bandit. The known arm pays $2 b$, the first
unknown arm pays $b + Y_t$, and the second unknown arm pays $Y_t^{(1)}
+ Y_t^{(2)}$, where $Y_t \sim \text{Bernoulli}(\theta)$ and
$Y_t^{(1)}, Y_t^{(2)} \sim \text{Bernoulli}(\theta)$ are conditionally
independent. This gives us
\begin{equation*}
  V_t(\alpha, \beta) = \max\cbrace{\delta^0_t, ~ \delta^1_t, ~ \delta^2_t},
\end{equation*}
where
\begin{equation*}
  \begin{aligned}
    \delta^0_t & = 2 b + \gamma V_{t + 1}(\alpha, \beta) \\
    \delta^1_t & = b + \frac{\alpha}{\alpha + \beta} +
    \gamma \paren{\frac{\alpha}{\alpha + \beta} V_{t + 1}(\alpha + 1,
      \beta) + \frac{\alpha}{\alpha + \beta} V_{t + 1}(\alpha, \beta +
      1)} \\
    \delta^2_t & = \frac{2 \alpha}{\alpha + \beta} + \gamma
    \left(\frac{\alpha (\alpha + 1)}{(\alpha + \beta) (\alpha + \beta
        + 1)} V_{t + 1}(\alpha + 2, \beta) \right. \\
    & \hspace{.775in} + \frac{2 \alpha \beta}{(\alpha + \beta)
      (\alpha + \beta + 1)} V_{t + 1}(\alpha + 1, \beta + 1) \\
    & \hspace{.775in} \left. + \frac{\beta (\beta + 1)}{(\alpha +
        \beta) (\alpha + \beta + 1)} V_{t + 1}(\alpha, \beta + 2)
    \right);
  \end{aligned}
\end{equation*}
$\delta_0$ represents the known arm, $\delta_1$ represents the first
unknown arm, and $\delta_2$ represents the second unknown arm. The pdf
of $Y_t^{(1)} + Y_t^{(2)}$ is known from homework~1. We compute the
value of the optimal policy for $T = 25$, $\alpha_0 = \beta_0 = 1$, $b
= .5$, and $\gamma = .95$ using the Python code below; we find
$V_0(\alpha_0, \beta_0) = 17.387$. Figure~\ref{fig:p5} shows the
states $(\alpha_t, \beta_t)$ accessible at time $t = 10$, and marks
the optimal action at that state.

\lstinputlisting[language=Python]{problem-5.py}

\noindent This code relies on the \texttt{two\_armed\_bandit} module,
listed in the appendix.

\begin{figure}[htp]
  \centering

  \includegraphics[width=\linewidth]{problem-5b.png}

  \caption{}
  \label{fig:p5}
\end{figure}

\section*{Appendix}

\lstinputlisting[language=Python]{bandit.py}

\lstinputlisting[language=Python]{two_armed_bandit.py}

\end{document}

%  LocalWords:  pdf
