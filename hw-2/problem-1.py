"""
File: problem-1.py

"""

from matplotlib import pyplot as plt
from matplotlib import rc
rc("font", **{"family": "serif"})
rc("text", usetex=True)

from bandit import Bandit

T = 25
alpha = 1
beta = 1
b = .5
gamma = .95

bandit = Bandit(T=T, alpha=alpha, beta=beta, b=b, gamma=gamma)

print(bandit.V(0, alpha, beta))

#
# Problem 1b; we plot i^* versus t
#

plt.scatter(range(0, T), bandit.istar())
plt.title("$T=%i, \\alpha=%i, \\beta=%i, b=%.1f, \\gamma=%.2f$" \
              % (T, alpha, beta, b, gamma))
plt.xlim((0 - .5, T - .5))
plt.ylim((0 - .5, max(bandit.istar()) + .5))
plt.xlabel("$t$")
plt.ylabel("$i^*$")
plt.savefig("problem-1b.png")
