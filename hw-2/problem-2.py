from random import Random

from bandit import Bandit

N = 1000
seed = 0
rand = Random(seed)
T = 25
alpha = 1
beta = 1
b = .5
gamma = .95
bandit = Bandit(T=T, alpha=alpha, beta=beta, b=b, gamma=gamma)

def trial(rand):
    """
    Simulate a trial of the policy

    """

    theta = rand.betavariate(alpha, beta)

    successes = 0
    reward = 0
    for t in range(0, T):
        _alpha = alpha + successes
        _beta = beta + t - successes
        if bandit.policy(t, _alpha, _beta) == bandit.UNKNOWN:
            if rand.random() <= theta:
                reward += gamma ** t
                successes += 1
        else:
            reward += gamma ** t * b

    return reward

trials = [trial(rand) for i in range(0, N)]

mean = sum(trials) / N
variance = sum((t - mean) ** 2 for t in trials) / (N - 1)

CI = 1.96 * (variance / N) ** .5

print("variance of the reward: %.3f" % variance)
print("reward at 95%% confidence: %.3f +- %.3f" % (mean, CI))
