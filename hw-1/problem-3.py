#!/usr/bin/python3

import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np
import scipy.stats as stats
from random import Random
import math

rc("font", **{"family": "serif"})
rc("text", usetex=True)

class Params:
    """ Container for problem parameters """

    def __init__(self, maxN=10, alpha=1, lam=1, mu=.5, b=.5, sigma=1, seed=0):
        self.maxN = maxN
        self.alpha = alpha
        self.lam = lam
        self.mu = mu
        self.b = b
        self.sigma = sigma

        rand = Random(seed)
        self.Y = [rand.normalvariate(mu, lam) for x in range(0, maxN)]

    def mu_prime(self, N):
        num = p.mu / self.sigma ** 2 + sum(self.Y[:N]) / self.lam ** 2
        den = 1 / self.sigma ** 2 + N / self.lam ** 2
        return  num / den

    def mu_prime_star(self, N):
        sps = self.sigma_prime(N) ** 2
        return - 1 / self.alpha * math.log(1 - self.b) + sps * self.alpha ** 2

    def sigma_prime(self, N):
        return 1 / (1 / self.sigma ** 2 + N / self.lam ** 2) ** .5

    def sigma_tilde(self, N):
        return (self.sigma ** 2 - self.sigma_prime(N) ** 2) ** .5

    def VOI(self, N):
        sig_p_sq = self.sigma_prime(N) ** 2
        sig_tilde = self.sigma_tilde(N)
        mu_prime = self.mu_prime(N)
        mu_star = self.mu_prime_star(N)

        if sig_tilde > 0:
            prob_1 = stats.norm.cdf((mu_star - self.mu) / sig_tilde)
            mean = (self.mu - self.alpha * sig_tilde ** 2)
            prob_2 = stats.norm.cdf((mu_star - mean) / sig_tilde)
        else:
            prob_1 = 1
            prob_2 = 1

        coef = math.exp(self.alpha ** 2 * (sig_p_sq - 1) / 2 - self.alpha)

        return 1 + prob_1 * (self.b - 1) - coef * (1 - prob_2)
    

#
# Problem 1a; we plot VOI(N) versus N
#

p = Params(maxN=100, alpha=1, lam=4, sigma=1, b=.5, mu=.6, seed=0)

plt.plot(range(0, p.maxN), [p.VOI(x) for x in range(0, p.maxN)])
plt.title("$\\alpha=%.1f, \\lambda=%.1f, \\sigma^2=%d, b=%.1f, \\mu=%.1f$" % \
              (p.alpha, p.lam, p.sigma ** 2, p.b, p.mu))
plt.xlabel("$N$")
plt.ylabel("VOI$(N)$")
plt.savefig("problem-3a.png")

#
# Problem 1b; we play VOI(N, alpha) versus alpha for fixed N
#

N = 50
alphas = np.arange(.1, 5, .05)
vois = []
for alpha in alphas:
    p.alpha = alpha
    vois.append(p.VOI(N))

plt.close()
plt.plot(alphas, vois)
plt.title("$N=%d, \\lambda=%.1f, \\sigma^2=%d, b=%.1f, \\mu=%.1f$" % \
              (N, p.lam, p.sigma ** 2, p.b, p.mu))
plt.xlabel("$\\alpha$")
plt.ylabel("VOI$(N)$")
plt.savefig("problem-3b.png")

