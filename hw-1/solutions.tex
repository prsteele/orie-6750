\documentclass{article}

\usepackage{graphicx}
\usepackage{prsteele}
\usepackage{fullpage}
\usepackage{listings}
\usepackage{algpseudocode}
\usepackage{psfrag}
\usepackage{url}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[babel=true]{microtype}

\usepackage{lmodern}

\title{ORIE 6250: Assignment 1}
\author{Patrick Steele}
\date{\today}

\lstset{%
  tabsize=2,
  showstringspaces=false
}

\newcommand{\VOI}{\text{VOI}}

\begin{document}

\maketitle

\section*{Problem 1}

Suppose that for given $a, b > 0$ that $\theta \sim \text{Beta}(a, b)$
and $Y_1, Y_2 \given \theta \sim \text{Bernoulli}(\theta)$, where
$Y_1$ and $Y_2$ are independent given $\theta$.

\begin{prop}
  The distribution of $Y_1$ is $\text{Bernoulli}(a / (a + b))$.
\end{prop}

\begin{proof}
  We first get the joint distribution of $Y_1$ and $\theta$, finding
  \begin{equation*}
    f(y_1, \theta) = f(y_1 | \theta) f(\theta) = \theta^{y_1} (1 -
    \theta)^{1 - y_1} \frac{\theta^{a - 1} (1 - \theta)^{b -
        1}}{\beta(a, b)} = \frac{\theta^{a + y_1 - 1} (1 - \theta)^{b
        - y_1 + 1 - 1}}{\beta(a, b)},
  \end{equation*}
  where $y_1 \in \cbrace{0, 1}$ and $\theta \in [0, 1]$. Then the
  distribution of $Y_1$ can be found by integrating out $\theta$, and
  we get
  \begin{equation*}
    f(y_1) = \int_0^1 \frac{\theta^{a + y_1 - 1} (1 - \theta)^{b
        - y_1 + 1 - 1}}{\beta(a, b)} \;d\theta = \frac{\beta(a + y_1,
      b - y_1 + 1)}{\beta(a, b)} = \frac{a^y b^{1 - y}}{a + b},
  \end{equation*}
  where $y_1 \in \cbrace{1, 0}$, since this is the kernel of a
  $\text{Beta}(a + y, b + 1 - y)$
  distribution. This is, of course, a $\text{Bernoulli}(a / (a + b))$
  distribution, as required.
\end{proof}

\begin{prop}
  The distribution of $Y_1 + Y_2$ is given by
  \begin{equation*}
    f(y) =
    \begin{cases}
      \frac{b (b + 1)}{(a + b + 1) (a + b)}, & y = 0 \\
      \frac{2 a b}{(a + b + 1) (a + b)}, & y = 1 \\
      \frac{a (a + 1)}{(a + b + 1) (a + b)}, & y = 2.
    \end{cases}
  \end{equation*}
\end{prop}

\begin{proof}
  Since $Y_1$ and $Y_2$ are independent given $\theta$, the joint
  distribution of $Y_1 + Y_2 | \theta$ is simply $\text{Binomial}{2,
    \theta}$. Then the joint distribution of $Y_1 + Y_2, \theta$ is
  \begin{equation*}
    f(y, \theta) = \binom{2}{y} \theta^y (1 - \theta)^{2 - y}
    \frac{\theta^{a - 1} (1 - \theta)^{b - 1}}{\beta(a, b)} =
    \frac{2}{y! (2 - y)!} \frac{\theta^{a + y - 1} (1 - \theta)^{b + 2
        - y - 1}}{\beta(a + b)},
  \end{equation*}
  where $y \in \cbrace{0, 1, 2}$ and $\theta \in [0, 1]$. Then the
  distribution of $Y_1 + Y_2$ can be found by integrating out
  $\theta$, and we get
  \begin{equation*}
    f(y) = \int_0^1 \frac{2}{y! (2 - y)!} \frac{\theta^{a + y - 1} (1
      - \theta)^{b + 2 - y - 1}}{\beta(a + b)} \; d\theta =
    \frac{2}{y! (2 - y)! \beta(a + b)} \beta(a + y, b + 2 - y),
  \end{equation*}
  where $y \in \cbrace{0, 1, 2}$, since this is the kernel of a
  $\text{Beta}(a + y, b + 2 - y)$ distribution. This expression
  simplifies to
  \begin{equation*}
    \frac{f(y)}{(a + b + 1) (a + b)} =
    \begin{cases}
      b (b + 1), & y = 0 \\
      2 a b, & y = 1 \\
      a (a + 1), & y = 2,
    \end{cases}
  \end{equation*}
as required.
\end{proof}

\clearpage
\section*{Problem 2}

Let $\theta \sim \text{Dirichlet}(\alpha)$ for $\alpha \in
\real^k_{++}$. Additionally, let $Y_i \in \cbrace{1, \ldots, k}$ be
such that $Y_1, \ldots, Y_n$ are iid given $\theta$, where the
distribution $Y_i \given \theta$ is given by $\prob\sbrace{Y_i = y
  \given \theta} = \theta_y$.

\begin{prop}
  The posterior distribution of $\theta$ is Dirichlet with parameter
  $\alpha'$, where $\alpha'_i = \sum_{j = 1}^n \ind(y_j = i) +
  \alpha_i$.
\end{prop}

\begin{proof}
  Since $Y_1, \ldots, Y_n$ are independent given $\theta$, we have
  that the joint distribution of $Y_1, \ldots, Y_n$ is given by
  \begin{equation*}
    f(y_1, \ldots, y_n) = \prod_{i = 1}^n \theta_{y_i} = \prod_{i =
      1}^k \theta_i^{\sum_{j = 1}^n \ind(y_j = i)},
  \end{equation*}
  where $y_i \in \cbrace{1, \ldots, k}$ for all $i$. Using Bayes'
  rule, we then find that
  \begin{equation*}
    f(\theta \given Y_1, \ldots, Y_n) \propto f(y_1, \ldots, y_n)
    f(\theta) = \prod_{i = 1}^k \theta_i^{\sum_{j = 1}^n \ind(y_j =
      i)}  \prod_{i = 1}^k \theta_i^{\alpha_i - 1} = \prod_{i = 1}^k
    \theta_i^{\sum_{j = 1}^n \ind(y_j = i) + \alpha_i - 1},
  \end{equation*}
  where $\theta \in \sbrace{0, 1}^k$ with $\sum_{i = 1}^k \theta =
  1$. This is the kernel of a Dirichlet distribution with parameter
  $\alpha'$, where $\alpha'_i = \sum_{j = 1}^n \ind(y_j = i) +
  \alpha_i$.
\end{proof}

\clearpage
\section*{Problem 3}

We consider the comparison to a standard problem were we are risk
averse; in particular, for some $\alpha > 0$ we choose $m(\theta) = 1
- \exp\cbrace{-\alpha \theta}$. We assume that $\theta \sim N(\mu,
\sigma^2)$ and $Y_i \sim N(\theta, \lambda^2)$. We wish to compute
$\VOI(N)$. To begin, we note that
\begin{equation*}
  v(a, \theta) =
  \begin{cases}
    m(\theta), & a = \text{``new''} \\
    b, & a = \text{``old''},
  \end{cases}
  \qquad
  v(N, Y, a) = \expectation\sbrace{v(a, \theta) \given N, Y} =
  \begin{cases}
    \expectation\sbrace{m(\theta) \given N, Y}, & a = \text{``new''}
    \\
    b, & a = \text{``old''}.
  \end{cases}
\end{equation*}
Since $\theta \given Y_1, \ldots, Y_n \sim N(\mu', {\sigma'}^2$ (from
previous work) where
\begin{equation*}
  \mu' = \frac{\frac{\mu}{\sigma^2} + \frac{1}{\lambda^2} \sum_{i =
      1}^n Y_i}{\frac{1}{\sigma^2} + \frac{N}{\lambda^2}}, \qquad
  {\sigma'}^2 = \sbrace{\frac{1}{\sigma^2} + \frac{N}{\sigma^2}}\inv,
\end{equation*}
we can compute
\begin{multline*}
  \expectation\sbrace{m(\theta) \given N, Y} = \int_{-\infty}^\infty
  \paren{1 - \exp\cbrace{-\alpha \theta}} \paren{2 \pi
    {\sigma'}^2}^{-1/2} \exp\cbrace{\frac{-\frac{1}{2} (\theta -
      \mu')^2}{{\sigma'}^2}} \; d\theta = \\
  1 - \int_{-\infty}^\infty \paren{2 \pi {\sigma'}^2}^{-1/2}
  e^{-\alpha \theta} \exp\cbrace{\frac{-\frac{1}{2} (\theta -
      \mu')^2}{{\sigma'}^2}} \; d\theta = 1 - M(-\alpha),
\end{multline*}
where $M(\cdot)$ is the moment generating function of a normal random
variable. Then
\begin{equation*}
  \expectation\sbrace{m(\theta) \given N, Y} = 1 - e^{-\alpha \mu'}
  e^{\frac{1}{2} {\sigma'}^2 \alpha^2} = 1 - \exp\cbrace{\frac{1}{2}
    {\sigma'}^2 \alpha^2 - \alpha \mu'}.
\end{equation*}
From here we can compute $v(N, Y) = \max\cbrace{b, 1 -
  \exp\cbrace{\frac{1}{2} {\sigma'}^2 \alpha^2 - \alpha
    \mu'}}$. Finally, we wish to compute
\begin{equation*}
  v(N) = \expectation\sbrace{v(N, Y) \given N}.
\end{equation*}
Recall from the notes that $\mu' \given N \sim N(\mu, \sigma^2 -
{\sigma'}^2)$. The maximum changes value when $\mu'$ is equal to
\begin{equation*}
  {\mu'}^* = -\frac{1}{\alpha} \ln(1 - b) + \frac{1}{2} {\sigma'}^2
  \alpha^2.
\end{equation*}
Thus,
\begin{equation*}
  \begin{alignedat}{2}
    v(N) & = \expectation\sbrace{v(N, Y) \given N} \\
    & = \int_{-\infty}^{{\mu'}^*} b \paren{2 \pi
      \tilde\sigma(N)^2}^{-1/2} \exp\cbrace{-\frac{1}{2
        \tilde\sigma(N)^2} (\mu' - \mu)^2} \; d\mu' \\
    & + \int_{{\mu'}^*}^\infty \paren{1 - \exp\cbrace{\frac{1}{2} {\sigma'}^2
      \alpha^2} \exp\cbrace{-\alpha \mu'}} \paren{2 \pi
      \tilde\sigma(N)^2}^{-1/2} \exp\cbrace{-\frac{1}{2
        \tilde\sigma(N)^2} (\mu' - \mu)^2} \; d\mu' \\
    & = \xi_1 - \xi_2.
  \end{alignedat}
\end{equation*}
The value of the first integral is simply $b \; \Phi\paren{\frac{{\mu'}^*
    - \mu}{\tilde\sigma(N)}} = \xi_1$, since the kernel is just that
of a normal distribution. By completing the square in the second
integral, we find
\begin{equation*}
  \begin{alignedat}{2}
    \xi_2 & = \int_{{\mu'}^*}^\infty \paren{1 -
      \exp\cbrace{\frac{1}{2} {\sigma'}^2 \alpha^2}
      \exp\cbrace{-\alpha \mu'}} \paren{2 \pi
      \tilde\sigma(N)^2}^{-1/2} \exp\cbrace{-\frac{1}{2
        \tilde\sigma(N)^2} (\mu' - \mu)^2} \; d\mu' \\
    & = \paren{1 - \Phi\paren{\frac{{\mu'}^* - \mu}{\tilde\sigma(N)}}}
    - \exp\cbrace{\frac{1}{2} {\sigma'}^2 \alpha^2}
    \int_{{\mu'}^*}^\infty \paren{2 \pi \tilde\sigma(N)^2}^{-1/2}
    \exp\cbrace{-\frac{1}{2
        \tilde\sigma(N)^2} (\mu' - \mu)^2 - \alpha \mu'} \; d\mu' \\
    & = \paren{1 - \Phi\paren{\frac{{\mu'}^* - \mu}{\tilde\sigma(N)}}}
    - \exp\cbrace{\frac{1}{2} {\sigma'}^2 \alpha^2}
    \int_{{\mu'}^*}^\infty \paren{2 \pi \tilde\sigma(N)^2}^{-1/2}
    \exp\cbrace{-\frac{1}{2} \paren{\mu' + \alpha \tilde\sigma(N)^2 -
        \mu}^2 - \frac{\alpha^2}{2} - \alpha} \\
    & = \paren{1 - \Phi\paren{\frac{{\mu'}^* - \mu}{\tilde\sigma(N)}}}
    - \exp\cbrace{\frac{1}{2} {\sigma'}^2 \alpha^2 -
      \frac{\alpha^2}{2} - \alpha} \paren{1 -
      \Phi\paren{\frac{{\mu'}^* - \paren{\mu - \alpha
            \tilde\sigma(N)^2}}{\tilde\sigma(N)}}}.
  \end{alignedat}
\end{equation*}
Thus, we have that
\begin{equation*}
  \begin{alignedat}{2}
    V(N) & = b \; \Phi\paren{\frac{{\mu'}^* - \mu}{\tilde\sigma(N)}}
    + \paren{1 - \Phi\paren{\frac{{\mu'}^* - \mu}{\tilde\sigma(N)}}} -
    \exp\cbrace{\frac{1}{2} \alpha^2 ({\sigma'}^2 - 1) -
      \alpha} \paren{1 - \Phi\paren{\frac{{\mu'}^* - \paren{\mu -
            \alpha
            \tilde\sigma(N)^2}}{\tilde\sigma(N)}}} \\
    & = \Phi\paren{\frac{{\mu'}^* - \mu}{\tilde\sigma(N)}} \paren{b -
      1} + 1 - \exp\cbrace{\frac{1}{2} \alpha^2 ({\sigma'}^2 - 1) -
      \alpha} \paren{1 - \Phi\paren{\frac{{\mu'}^* - \paren{\mu -
            \alpha \tilde\sigma(N)^2}}{\tilde\sigma(N)}}}
  \end{alignedat}
\end{equation*}
and so finally we have
\begin{equation*}
  \VOI(N) = \Phi\paren{\frac{{\mu'}^* - \mu}{\tilde\sigma(N)}} \paren{b -
    1} + 1 - \exp\cbrace{\frac{1}{2} \alpha^2 ({\sigma'}^2 - 1) -
    \alpha} \paren{1 - \Phi\paren{\frac{{\mu'}^* - \paren{\mu -
          \alpha \tilde\sigma(N)^2}}{\tilde\sigma(N)}}} - \max\cbrace{\mu, b}.
\end{equation*}

In figure~\ref{fig:3a} we plot $\VOI(N)$ versus $N$, while in
figure~\ref{fig:3b} we plot $\VOI(N)$ versus $\alpha$ for a fixed
$N$. The code used to generate these figures is listed below.

\lstinputlisting[language=Python]{problem-3.py}

\begin{figure}[htp]
  \centering

  \includegraphics[width=.8\linewidth]{problem-3a.png}

  \caption{A plot of $\VOI(N)$ versus $N$ for parameter values as
    described in the plot.}
  \label{fig:3a}
  
\end{figure}

\begin{figure}[htp]
  \centering

  \includegraphics[width=.8\linewidth]{problem-3b.png}

  \caption{A plot of $\VOI(N)$ versus $\alpha$ for parameter values as
    described in the plot.}
  \label{fig:3b}
  
\end{figure}

\clearpage
\section*{Problem 4}

We employ bisection to compute the optimal value of $N^*$ in the
comparison to the standard problem from the notes. The code used to do
so is shown below; to utilize this code, one would initialize an
instance \texttt{p} of the \texttt{Params} class with the relevant
parameters, and then call \texttt{p.maximize(c)} to compute the optimal
$N^*$ at \texttt{c}.

\lstinputlisting[language=Python]{problem-4.py}

\clearpage
\section*{Problem 5}

We compute the value of information for the model given. We begin with
$V(a, theta) = -(a - \theta)^2$ as given. We then compute $V(N, Y,
a)$, finding
\begin{multline*}
  V(N, Y, a) = \expectation\sbrace{V(a, \theta) \given N, Y} =
  \expectation\sbrace{-a^2 - \theta^2 + 2 a \theta \given N, Y} \\
  = -a^2 - (\var(\theta \given N, Y) + \expectation\sbrace{\theta
    \given N, Y}) + 2 a \expectation\sbrace{\theta \given N, Y} = -a^2
  - ({\sigma'}^2 - {\mu'}^2) + 2 a \mu',
\end{multline*}
since $\theta \given N, Y \sim N(\mu', {\sigma'}^2)$ where $\mu'$ and
$\sigma'$ are defined as above. Next we compute $V(N, Y)$ by
maximizing over $a$; to do this, we observe that $V(N, Y, a)$ is
concave, and so we simply find the zero of the derivative which occurs
at $a = \mu'$. Thus, we have
\begin{equation*}
  V(N, Y) = -{\mu'}^2 + 2 {\mu'}^2 - {\sigma'}^2 - {\mu'}^2 =
  -{\sigma'}^2.
\end{equation*}
Interestingly computing $V(N)$ is simple, as $V(N, Y)$ does not depend
on the data, and so $V(N) = V(N, Y)$. Finally, we compute $\max_a
\expectation V(a, \theta)$. As before, we have $\expectation V(a,
\theta) = 2 a \mu - a^2 - (\sigma^2 + \mu^2)$, which is maximized by
choosing $a = \mu$. Thus,
\begin{equation*}
  \max_a \expectation V(a, \theta) = 2 \mu^2 - \mu^2 - \sigma^2 -
  \mu^2 = \sigma^2,
\end{equation*}
and so
\begin{equation*}
  \VOI(N) = {\sigma'}^2 - \sigma^2,
\end{equation*}
which is $\tilde\sigma^2$ using our previous notation.
\end{document}
