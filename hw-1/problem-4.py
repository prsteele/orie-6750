#!/usr/bin/python3

import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np
import scipy.stats as stats
from random import Random

class Params:
    """ Container for problem parameters """

    def __init__(self, maxN=10, alpha=1, lam=1, mu=.5, b=.5, sigma=1, c=1, seed=0):
        self.maxN = maxN
        self.alpha = alpha
        self.lam = lam
        self.mu = mu
        self.b = b
        self.c = c
        self.sigma = sigma

        rand = Random(seed)
        self.Y = [rand.normalvariate(mu, lam) for x in range(0, maxN)]

    def sigma_prime(self, N):
        return 1 / (1 / self.sigma ** 2 + N / self.lam ** 2) ** .5

    def sigma_tilde(self, N):
        return (self.sigma ** 2 - self.sigma_prime(N) ** 2) ** .5

    @staticmethod
    def f(a):
        return a * stats.norm.cdf(a) + stats.norm.pdf(a)

    def V(self, N):
        sig_tilde = self.sigma_tilde(N)
        t1 = max(self.mu, self.b)
        t2 = sig_tilde * self.f(-abs(self.mu - self.b) / sig_tilde)
        t3 = - self.c * N
        return t1 + t2 + t3

    def maximize(self, c):
        """
        Computes the optimal value of N^* for the given cost parameter c
        
        """
        _last_c = self.c
        self.c = c
        err = 1e-10
        last = 0
        U = 1
        while self.V(U) > last:
            U *= 2

        L = err
        vL = self.V(L)
        vU = self.V(U)

        while abs(vU - vL) > err:
            M = (L + U) / 2
            vM = self.V(M)

            if vM <= vU:
                L, vL = M, vM
            else:
                U, vU = M, vM

        self.c = _last_c
        return (L + U) / 2

#
# Problem 4
#

p = Params(maxN=200, alpha=1, lam=4, sigma=1, b=.5, mu=.6, c=.01, seed=0)

cs = np.arange(.001, .05, .0005)

plt.plot(cs, [p.maximize(c) for c in cs])
plt.title("$\\alpha=%.1f, \\lambda=%.1f, \\sigma^2=%d, b=%.1f, \\mu=%.1f$, c=%.1f" % \
              (p.alpha, p.lam, p.sigma ** 2, p.b, p.mu, p.c))
plt.xlabel("$c$")
plt.ylabel("V$(N)$")
plt.savefig("problem-4.png")

