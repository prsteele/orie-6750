import networkx as nx
import random
import sys

if len(sys.argv) != 3:
    print('usage: python3 %s epsilon outfile' % sys.argv[0])
    sys.exit(1)

epsilon = float(sys.argv[1])
if epsilon < 0 or epsilon > 1:
    raise ValueError('The epsilon value must be in [0, 1], not %.3f' % epsilon)

from graphbandit import Bandit, EpsilonGreedy, ExponentialGammaDist
from graphbandit.G5.trim import trim
from graphbandit.G5.GSmaller import G, S, t

seed = 0
rand = random.Random(seed)
n = 10
policy = EpsilonGreedy(epsilon, rand)
dist = ExponentialGammaDist(rand)
T = 40
b = Bandit(trim(G, S, t), S, t, dist, policy, n)

while b.T < T:
    b.advance()

fig = b.plot_posterior_means()
fig.axes[0].set_ylim((0, 10))
title = 'The epsilon-greedy heuristic with $\\epsilon=%.2f$' % epsilon
fig.axes[0].set_title(title)

fig.savefig(sys.argv[2])

print(sum(b.values))
