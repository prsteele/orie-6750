import sys

if len(sys.argv) != 2:
    print("Usage: python3 plot_degrees.py OUTPUT_FILE.png")
    sys.exit(1)
output_file = sys.argv[1]

print("loading...")
from graphbandit.G5.g5_graph import G
print("loaded")

import matplotlib.pyplot as plt
from matplotlib import rc
import networkx as nx

rc("text", usetex=True)

degrees = list(nx.degree(G).values())

plt.hist(degrees, log=True, normed=True, bins=100)
plt.xlabel("Node degree")
plt.ylabel("Log-frequency")
plt.title("Node degree frequency")
plt.savefig(output_file)
