from graphbandit.G5.GSmaller import G, S, t

print("digraph G {")
print("ranksep=10;")
print("ratio=auto;")
print('root="%s";' % t)
for n in G:
    if n in S:
        print('"%s" [label="",height=1,width=1,fixedsize=true,color=blue,style=filled];' % n)
    elif n == t:
        print('"%s" [label="",height=1,width=1,fixedsize=true,color=red,style=filled];' % n)
    else:
        print('"%s" [label="",height=.1,width=.1,fixedsize=true];' % n)

for (u, v) in G.edges_iter():
    print('"%s" -> "%s" [label=" "];' % (u, v))
        
print("}")
