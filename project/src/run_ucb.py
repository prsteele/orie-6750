import networkx as nx
import random
import sys

if len(sys.argv) != 2:
    print("usage: python3 %s outfile" % sys.argv[0])
    sys.exit(1)

from graphbandit import Bandit, UCB, ExponentialGammaDist
from graphbandit.G5.trim import trim
from graphbandit.G5.GSmaller import G, S, t

seed = 0
rand = random.Random(seed)
n = 10
policy = UCB()
dist = ExponentialGammaDist(rand)
T = 40
b = Bandit(trim(G, S, t), S, t, dist, policy, n)

while b.T < T:
    b.advance()

fig = b.plot_posterior_means()
fig.axes[0].set_ylim((0, 10))
fig.axes[0].set_title('The uniform upper (lower) bound heuristic')

fig.savefig(sys.argv[1])

print(sum(b.values))
