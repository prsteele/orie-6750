from graphbandit import Policy

class UCB(Policy):
    """
    An upper confidence bound heuristic; since we are minimizing, it
    is in fact a _lower_ confidence bound.

    """

    def choose_arm(self, G, S, t, pmean, pvariance):
        """
        Choose an arm to pull. With probability 1 - self.epsilon,
        choose the arm with the highest posterior mean. pmean and
        pvariance are dictionaries mapping elements of S to their
        posterior mean and posterior variance, respectively.

        """

        return min(S, key=lambda k: pmean[k][-1] -
                   1.96 * pvariance[k][-1] ** .5)
            
