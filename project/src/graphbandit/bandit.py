import random
import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cm
import numpy as np
from matplotlib import rc, rcParams
from graphbandit.G5.GSmaller.true_mean import true_mean
rc('text', usetex=True)
rcParams.update({'figure.autolayout': True})

class Bandit:
    """
    This class represents a multi-armed bandit for the network
    communication problem.

    """

    def __init__(self, G, S, t, dist, policy, n, noposterior=False):
        """
        Construct a new Bandit where edge lengths are generated and
        updated using Dist `dist`, and our decisions come from the
        Policy `policy`. We wish to send information to node t from
        nodes in S. When we estimate via Monte Carlo simulation we
        run `n` trials.

        If noposterior is False, we don't bother simulating the
        posterior distribution.
        
        """
        
        self.G = G
        self.S = S
        self.t = t
        self.n = n

        self.dist = dist
        self.policy = policy

        self.T = 0
        self.arms = []
        self.values = []
        self.pmean = {s: [] for s in self.S}
        self.pvariance = {s: [] for s in self.S}

        self._has_realized_posterior = False

        # Get priors on every edge
        for (u, v, data) in G.edges_iter(data=True):
            self.dist.setup_edge((u, v), data)

        self.noposterior = noposterior

        # Get an initial posterior estimate (i.e., the prior estimate)
        self.estimate_posterior()

    def realize(self):
        """
        Generates a realization of each edge

        """

        for (u, v, data) in sorted(self.G.edges_iter(data=True)):
            data['L'] = self.dist.realize_length((u, v), data)
    
    def pull(self, s):
        """
        Compute the shortest (s, t) path in the realized network,
        updates the posteriors of observed edges, and returns the
        value of the path.

        """

        if s not in self.S:
            raise ValueError("Unknown arm %s" % s)

        self.arms.append(s)

        p = nx.shortest_path(self.G, s, self.t, "L")

        L = 0

        i = 0
        for (u, v) in zip(p, p[1:]):
            x = self.G.edge[u][v]["L"]
            self.dist.update_posterior((u, v), self.G.edge[u][v], x)
            L += x
            i += 1

        self.values.append(L)

    def advance(self):
        """
        Simulate one additional time period.

        """

        # Generate a realization of the network, pull an arm, and
        # update our beliefs
        self.realize()
        self.pull(self.policy.choose_arm(self.G, self.S, self.t,
                                         self.pmean, self.pvariance))
        self.estimate_posterior()
        self.T += 1

    def estimate_posterior(self):
        """
        Estimates the posterior mean and variance of the shortest
        (s, t) path for each s in self.S via Monte Carlo simulation.
        The estimates are appended to self.pmean and self.pvariance.
       
        """

        if self.noposterior:
            return

        # Maps each s to a list of the lengths observed in simulation
        L = {}

        for s in self.S:
            L[s] = []

        for i in range(0, self.n):
            for (u, v, data) in sorted(self.G.edges_iter(data=True)):
                data['posterior-L'] = self.dist.realize_posterior((u, v), data)

            self.G.reverse(copy=False)
            lens = nx.single_source_dijkstra_path_length(self.G, self.t, weight='posterior-L')
            self.G.reverse(copy=False)
            
            for s in self.S:
                try:
                    d = lens[s]
                except:
                    d = float('inf')

                L[s].append(d)

        means = {s: sum(L[s]) / self.n for s in self.S}
        variances = {s: sum((x - means[s]) ** 2 for x in L[s]) / (self.n - 1)
                     for s in self.S}

        for s in self.S:
            self.pmean[s].append(means[s])
            self.pvariance[s].append(variances[s])

    def plot_posterior_means(self):
        """
        Return a matplotlib.pyplot.Figure which shows the
        posterior means over time.

        """
        
        fig = plt.figure(figsize=(8, 4), dpi=300)
        ax = fig.add_subplot(111)
        
        rainbow = cm.get_cmap('rainbow')
        c_norm = colors.Normalize(vmin=0, vmax=self.T)
        
        scalar_map = cm.ScalarMappable(norm=c_norm, cmap=rainbow)
        scalar_map.set_array(range(0, self.T))
        
        X = sorted(self.S)
        scale = 4
        xvals = np.arange(0, len(X)) * scale
        for i in range(0, self.T + 1):
            c = scalar_map.to_rgba(i)
            yerr = [1.96 * (self.pvariance[x][i] / (self.n - 1)) ** .5 for x in X]
            
            ax.errorbar(xvals + scale * i / (self.T + 1), [self.pmean[x][i] for x in X],
                        yerr, None, color=c, linestyle='none')

            if i > 0:
                arm = self.arms[i - 1]
                ax.plot([xvals[X.index(arm)] + scale * i / (self.T + 1)], [self.pmean[arm][i]],
                        marker='x', color=c, markersize=10)

        # Add in the true estimated mean as a black line
        ax.errorbar(xvals + scale / 2, [true_mean[s] for s in X], None, [2] * len(X),
                    color='k', linestyle='none')
            
        ax.set_xticks(xvals)
        ax.set_xticklabels(X)
        ax.set_ylabel('Posterior mean')
        plt.xticks(rotation=-90)
        plt.colorbar(scalar_map, ticks=range(0, self.T + 1, 2))
        fig.axes[1].set_title('Iterations')            
        ax.set_xlim((-.5, max(xvals) + .5))

        return fig
