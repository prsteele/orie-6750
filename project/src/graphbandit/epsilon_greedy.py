from graphbandit import Policy

class EpsilonGreedy(Policy):
    """
    An epsilon-greedy policy.

    """

    def __init__(self, epsilon, rand):
        """
        Creates a new epsilon-greedy policy using `rand` as a
        random number source.

        """

        self.epsilon = epsilon
        self.rand = rand

    def choose_arm(self, G, S, t, pmean, pvariance):
        """
        Choose an arm to pull. With probability 1 - self.epsilon,
        choose the arm with the highest posterior mean. pmean and
        pvariance are dictionaries mapping elements of S to their
        posterior mean and posterior variance, respectively.

        """

        if self.rand.random() < self.epsilon:
            return self.rand.choice([s for s in S if pmean[s][-1] < float('inf')])
        else:
            return min(S, key=lambda k: pmean[k][-1])
