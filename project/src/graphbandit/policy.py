class Policy:
    """
    This class represents a policy for choosing which arm to pull
    in a Bandit.

    """

    def choose_arm(self, G, S, t, pmean, pvariance):
        """
        Choose an arm in S to pull given our knowledge of G.
        pmean and pvariance are maps from elements of S to a list
        of posterior mean and posterior variance estimates at each
        time.

        """

        raise NotImplementedError()
