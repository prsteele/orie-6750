from graphbandit import Policy

class SinglePull(Policy):
    """
    A heuristic which pulls only a single arm

    """

    def __init__(self, arm):
        self.arm = arm

    def choose_arm(self, G, S, t, pmean, pvariance):
        return self.arm
            
