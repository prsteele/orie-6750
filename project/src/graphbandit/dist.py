class Dist:
    """
    This class represents a distribution of edge lengths
    and an associated prior.

    """

    def setup_edge(self, e, data):
        """
        Compute the true and prior distributions on e and
        store the results in data.

        """

        raise NotImplementedError()

    def realize_length(self, e, data):
        """
        Return a realization of the length of edge e in graph G
        according to the true distribution on e.

        """

        raise NotImplementedError()

    def realize_posterior(self, e, data):
        """
        Return a realization of the length of edge e in graph G
        according to the posterior distribution on e.

        """

        raise NotImplementedError()

    def update_posterior(self, e, data, x):
        """
        Update the posterior distribution of the length
        of e in graph G given a realization x.

        """

        raise NotImplementedError()
