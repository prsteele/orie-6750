"""

This module defines a graph G = (V, E) where V is the set of users
from the G5 dataset and E is the set of all edges (u, v) such that u
sends v a message.

"""

import networkx as nx
import csv
import os
import time

__all__ = ["G"]

cd = os.path.dirname(__file__)

data_dir = os.path.join(cd, "data/G5/ydata-ymessenger-user-communication-pattern-v1_0")

columns = {"Date": 0,
           "Time": 1,
           "Sender": 2,
           "Zip": 3,
           "Receiver": 4,
           "IsFriend": 5}

if not os.path.exists(data_dir):
    raise FileNotFoundError("Run `scons -u` in the G5 directory to generate data")

# Construct a digraph where each user is a node and there is an edge
# (u, v) if u communicates with v. We attach message data to the
# "data" attribute of each edge.
G = nx.DiGraph()

# Read in all data
for fname in sorted(f for f in os.listdir(data_dir) if f[:5] == "ydata"):
    with open(os.path.join(data_dir, fname)) as f:
        reader = csv.reader(f, delimiter=' ')
        for row in reader:
            (u, v) = (row[columns["Sender"]], row[columns["Receiver"]])

            (h, m, s) = [int(x) for x in row[columns["Time"]].split(":")]
            t = h * 3600 + m * 60 + s

            if t == 0:
                # Instantaneous communications shouldn't happen
                t = 1

            if G.has_edge(u, v):
                G[u][v]["data"].append(t)
            else:
                G.add_edge(u, v, data=[t])
