from networkx import nx

def trim(G, S, t):
    """
    Returns a subgraph of G that only includes edges on a (s, t)
    path for all s in S.

    """

    H = nx.DiGraph()

    for s in S:
        T_forward = nx.bfs_tree(G, s)
        T_backward = nx.bfs_tree(G, t, reverse=True)

        for n in T_forward:
            if n in T_backward:
                H.add_node(n)

    for (u, v) in G.edges_iter():
        if u in H and v in H:
            H.add_edge(u, v, G[u][v])

    return H
