from graphbandit.G5.GSmaller.edges import E
from graphbandit.G5.means import means

import networkx as nx

__all__ = ['G']

G = nx.DiGraph()
G.add_edges_from(E)

for (u, v, data) in G.edges_iter(data=True):
    data['mean'] = means[(u, v)]
