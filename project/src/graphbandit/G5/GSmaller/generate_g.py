"""
This class writes to standard out a subgraph of G5.G, 

"""

from G5.g5_graph import G

import networkx as nx

users = sorted(G.nodes())
T = list(nx.bfs_edges(G, users[0]))
V = [T[0][0]]
for (u, v) in T:
    if len(V) < 1000:
        V.append(v)
    else:
        break

E = []
for u in V:
    for v in V:
        if G.has_edge(u, v):
            E.append((u, v))

print("E = [")
for (u, v) in E:
    print("('%s', '%s')," % (u, v))
print("]")
     
