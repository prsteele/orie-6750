from g5_graph import G

print("means = {")
for (u, v, data) in G.edges_iter(data=True):

    extra = (28 - len(data['data'])) * 24 * 60 * 60
    m = ((extra + sum(data['data'])) / 28) / (24 * 60 * 60)
    print("('%s', '%s'): %f," % (u, v, m))
print("}")
