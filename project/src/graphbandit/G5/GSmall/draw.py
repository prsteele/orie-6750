from small import G

print("digraph G {")
print("ranksep=5;")
print("ratio=auto;")

for n in G:
    print('"%s" [label="",height=.1,width=.1,fixedsize=true];' % n)

for (u, v) in G.edges_iter():
    print('"%s" -> "%s" [label=" "];' % (u, v))
        
print("}")
