"""
This module loads all G5 data into a Pandas data frame, stored in the
module variable `data'.

"""

import os
import pandas as pd
import datetime

__all__ = ["G5", "g5_time"]

cd = os.path.dirname(__file__)

data_dir = os.path.join(cd, "data/G5/ydata-ymessenger-user-communication-pattern-v1_0")

column_names = ["Date", "Time", "Sender", "Zip", "Receiver", "IsFriend"]

if not os.path.exists(data_dir):
    raise FileNotFoundError("Run `scons -u` in the G5 directory to generate data")

# Load up all data
G5 = pd.DataFrame(columns=column_names)
for fname in sorted(os.listdir(data_dir)):
    # Skip all files that aren't of the form ydata*.txt
    if fname[:5] != "ydata":
        continue

    print(fname)

    with open(os.path.join(data_dir, fname)) as f:
        d = pd.read_table(f, sep=" ", header=None, names=column_names)

        G5 = G5.append(d)

def g5_time(row):
    """
    Returns a datetime.timedelta object representing the time of the
    message in the given row, relative to the day it was sent.

    """

    (hours, minutes, seconds) = [int(x) for x in row["Time"].split(":")]
    d = datetime.timedelta(hours=hours, minutes=minutes, seconds=seconds)
    return d
