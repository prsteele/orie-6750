from graphbandit import Dist

class ExponentialGammaDist(Dist):
    """
    This class represents a model where edges have
    exponentially distributed lengths, where the prior
    distribution on the mean is gamma distributed.

    """

    def __init__(self, rand):
        """
        Creates a new exponential-gamma model using `rand` as a
        source of randomness.

        """

        self.rand = rand

    def setup_edge(self, e, data):
        """
        Compute and store the prior on e, as well as the true distribution.

        """

        data['prior'] = [1, 1]

    def realize_length(self, e, data):
        """
        Realize the length of e

        """

        return self.rand.expovariate(1/data['mean'])

    def realize_posterior(self, e, data):
        """
        Returns a sample from the posterior distribution on e.

        """

        (a, b) = data["prior"]
        mean = self.rand.gammavariate(a, 1/b)
        return self.rand.expovariate(1/mean)
    
    def update_posterior(self, e, data, x):
        """
        Update the posterior distribution on edge e given
        realization x.

        """

        data["prior"][0] += 1
        data["prior"][1] += x
