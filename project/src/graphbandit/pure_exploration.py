from graphbandit import Policy

class PureExploration(Policy):
    """
    A policy that always pulls a random arm.

    """

    def __init__(self, rand):
        """
        Constructor.

        """

        self.rand = rand

    def choose_arm(self, G, S, t, pmean, pvariance):
        """
        Choose an arm to pull.

        """

        return self.rand.choice([s for s in S if pmean[s][-1] < float('inf')])
