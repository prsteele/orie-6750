import networkx as nx
import random
import sys

from graphbandit import Bandit, SinglePull, ExponentialGammaDist
from graphbandit.G5.trim import trim
from graphbandit.G5.GSmaller import G, S, t

seed = 0
rand = random.Random(seed)
n = 2
dist = ExponentialGammaDist(rand)
T = 500

true_mean = {s: 0 for s in S}
variance = {s: 0 for s in S}

for s in S:
    print(s)
    policy = SinglePull(s)
    try:
        b = Bandit(trim(G, S, t), S, t, dist, policy, n, noposterior=True)

        while b.T < T:
            b.advance()

        true_mean[s] = sum(b.values) / len(b.values)
        mean = true_mean[s]
        variance[s] = sum((x - mean) ** 2 for x in b.values) / (len(b.values) - 1)
    except Exception as e:
        print('arm %s: %s' % (s, str(e)))
        true_mean[s] = float('inf')

print('\\begin{tabular}{l|l}')
for s in sorted(S):
    print('%s & %.3f \\pm %.3f \\\\' % (s, true_mean[s], 1.96 * (variance[s] / T)**.5))
print('\\end{tabular}')
