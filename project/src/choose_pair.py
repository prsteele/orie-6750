from G5.GSmaller import G

import networkx as nx
import random

seed = 0
rand = random.Random(seed)

nsamples = 2000
max_pair = None
max_dist = 0
users = G.nodes()
for i in range(0, nsamples):
    s = rand.choice(users)
    t = rand.choice(users)
    try:
        sp = nx.shortest_path(G, s, t)

        if len(sp) > max_dist:
            max_pair = (s, t)
            max_dist = len(sp)
    except:
        continue

# Now find the first 100 children of s
s = max_pair[0]
S = set([s])

bfs = nx.bfs_edges(G, s)

for (u, v) in nx.bfs_edges(G, s):
    S.add(u)
    S.add(v)

    if len(S) >= 12:
        break

print(sorted(S))
print(max_pair[1])
print(max_dist)
